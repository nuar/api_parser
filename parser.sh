#!/bin/bash
cp $1 api2.yaml
cat api2.yaml

#-------------------
#Test version format
#-------------------
x=$(cat api2.yaml | yq '.' | grep "x-ibm-configuration")
if [[ -z $x ]]
then
	echo "ERROR: Does not exists header x-ibm-configuration, it is necesary to validate the api yaml"
exit 1
fi

#-----------------
#Boolean variables
#-----------------
bool=0
boolVersion=0
boolApiProperties=0
boolApiPolicies=0
boolApiDefinitions=0
boolApiSecurityDefinitions=0


	
#Replace in the new yaml file created x-ibm-configuration with xibmconfiguration, now we can use yq command.
find ./api2.yaml -type f -exec sed -i 's/x-ibm-configuration/xibmconfiguration/g' {} +

#-------------------
#Test version format
#-------------------
echo "--- Test version format ---"
version=$(cat api2.yaml | yq .info.version)
echo "Version: $version"
Mayor=$(echo $version | cut -d\" -f2 | cut -d. -f1)
echo "Mayor: $Mayor"
minor=$(echo $version | cut -d. -f2)
echo "minor: $minor"
patch=$(echo $version | cut -d\" -f2 | cut -d. -f3)
echo "patch: $patch"
re='^[0-9]+$'
if ! [[ $Mayor =~ $re ]] ; then
   echo "error: Mayor version is not a number"
   bool=1
   boolVersion=1
fi
if ! [[ $minor =~ $re ]] ; then
   echo "error: minor version is not a number"
   bool=1
   boolVersion=1
fi
if ! [[ $patch =~ $re ]] ; then
   echo "error: patch version is not a number"
   bool=1
   boolVersion=1
fi



   
#-------------------   
#Test api properties
#-------------------
echo "--- Test api properties ---"
entorno=$(cat api2.yaml | yq .xibmconfiguration.properties.Entorno)
echo "Entorno: $entorno"
if [ "$entorno" == "null" ]; then
      echo "ERROR: Entorno does not exist"
	  bool=1
	  boolApiProperties=1
fi
                
sistema=$(cat api2.yaml | yq .xibmconfiguration.properties.Sistema)
echo "Sistema: $sistema"
if [ "$sistema" == "null" ]; then
      echo "ERROR: Sistema does not exist"
	  bool=1
	  boolApiProperties=1
fi
                
subsistema=$(cat api2.yaml | yq .xibmconfiguration.properties.Subsistema)
echo "Subsistema: $subsistema"
if [ "$subsistema" == "null" ]; then
        echo "ERROR: Subsistema does not exist"
fi
aplicacion=$(cat api2.yaml | yq .xibmconfiguration.properties.Aplicacion)
echo "Aplicacion: $aplicacion"
if [ "$aplicacion" == "null" ]; then
        echo "ERROR: Aplicacion does not exist"
		bool=1
	    boolApiProperties=1
fi
subaplicacion=$(cat api2.yaml | yq .xibmconfiguration.properties.Subaplicacion)
echo "Subaplicacion: $subaplicacion"
if [ "$subaplicacion" == "null" ]; then
        echo "ERROR: Subaplicacion does not exist"
		bool=1
	    boolApiProperties=1
fi
subaplicacionversion=$(cat api2.yaml | yq .xibmconfiguration.properties.SubaplicacionVersion)
echo "Subaplicacion: $subaplicacionversion"
if [ "$subaplicacionversion" == "null" ]; then
        echo "ERROR: SubaplicacionVersion does not exist"
		bool=1
	    boolApiProperties=1
fi
backend=$(cat api2.yaml | yq .xibmconfiguration.properties.backend)
echo "Backend: $backend"
if [ "$backend" == "null" ]; then
        echo "ERROR: backend does not exist"
		bool=1
	    boolApiProperties=1
fi
languageES=$(cat api2.yaml | yq .xibmconfiguration.properties.languageES)
echo "languageES: $languageES"
if [ "$languageES" == "null" ]; then
        echo "ERROR: languageES does not exist"
		bool=1
	    boolApiProperties=1
fi
channel=$(cat api2.yaml | yq .xibmconfiguration.properties.channel)
echo "channel: $channel"
if [ "$channel" == "null" ]; then
        echo "ERROR: languageES does not exist"
		bool=1
	    boolApiProperties=1
fi
href_sos_help=$(cat api2.yaml | yq .xibmconfiguration.properties.href_sos_help)
echo "href_sos_help: $href_sos_help"
if [ "$href_sos_help" == "null" ]; then
        echo "ERROR: href_sos_help does not exist"
		bool=1
	    boolApiProperties=1
fi
SOSgateway=$(cat api2.yaml | yq .xibmconfiguration.properties.SOSgateway)
echo "SOSgateway: $SOSgateway"
if [ "$SOSgateway" == "null" ]; then
        echo "ERROR: SOSgateway does not exist"
		bool=1
	    boolApiProperties=1
fi





#-------------------
#Test api policies
#-------------------
echo "--- Test api policies ---"
tlogger=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "technical-logger")
if [ -z ${tlogger+x} ]; then 
	
	echo "ERROR: technical-logger policy does not exist"
	bool=1
	boolApiPolicies=1 
 
fi
oauth=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "sg-oauth2_validate")
if [ -z ${oauth+x} ]; then 
	
	echo "ERROR: sg-oauth2_validate policy does not exist"
	bool=1
	boolApiPolicies=1 
 
fi
token1=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "token-jwt")
token2=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "token-bks")
if [ -z ${token1+x} -a -z ${token2+x} ]; then 
	
	echo "ERROR: token-jwt and token-bks  policies do not exist"
	bool=1
	boolApiPolicies=1 
 
fi
headerm=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "header-manager")
if [ -z ${headerm+x} ]; then 
	
	echo "ERROR: header-manager policy does not exist"
	exit 1 
 
fi
invoke=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "invoke")
if ! [ -z ${headerm+x} ]; then 
	
	echo "Invoke policy is active so we need to test if the need catches and policies exists too"
	echo "Test if OperationError exists"
	oe=$(cat api2.yaml | yq '.xibmconfiguration.assembly.catch' | grep "OperationError")
	if [ -z $oe ]; then 
	echo "ERROR: OperationError catch is needed"
	bool=1
	boolApiPolicies=1 
	fi
	echo "Test if SOAPError exists"
	se=$(cat api2.yaml | yq '.xibmconfiguration.assembly.catch' | grep "SOAPError")
	if [ -z $se ]; then 
	echo "ERROR: SOAPError catch is needed"
	bool=1
	boolApiPolicies=1 
	fi
	
	echo "Test if Set-ctx-msg-body exists"
	ctx=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "set-reasonPhrase")
	if [ -z ${ctx+x} ]; then 
	echo "ERROR:  Set-ctx-msg-body policy does not exist"
	bool=1
	boolApiPolicies=1 
	fi
	
	echo "Test if MSErr2Consumer  or ZuulErr2Consumer policies exists"
	Consumer1=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "MSErr2Consumer")
	Consumer2=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "ZuulErr2Consumer")
	if [ -z ${Consumer1+x} -a -z ${Consumer2+x} ]; then 
	echo "ERROR:  MSErr2Consumer policy and ZuulErr2Consumer  do not exist"
	bool=1
	boolApiPolicies=1 
	fi
	echo "Test if set-ctx-msg-body exists"
	ctx=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "set-ctx-msg-body")
	if [ -z ${ctx+x} ]; then 
	echo "ERROR:  set-reasonPhrase policy does not exist"
	bool=1
	boolApiPolicies=1 
	fi
	
	echo "Test if set-reasonPhrase exists"
	phrase=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "set-reasonPhrase")
	if [ -z ${phrase+x} ]; then 
	echo "ERROR:  set-reasonPhrase policy does not exist"
	bool=1
	boolApiPolicies=1 
	fi
	echo "Test if set-reasonPhrase exists"
	sc=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "SOAPErr2Consumer")
	if [ -z ${sc+x} ]; then 
	echo "ERROR:  SOAPErr2Consumer policy does not exist"
	bool=1
	boolApiPolicies=1 
	fi
 
fi
	echo "Test if activity-logger exists"
	al=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "activity-logger")
	if [[ -z $al ]] 
	then 
	echo "ERROR:  activity-logger policy does not exist"
	bool=1
	boolApiPolicies=1 
	fi
	
	echo "Test if activity-logger exists"
	fl=$(cat api2.yaml | yq '.xibmconfiguration.assembly.execute' | grep "functional-logger")
	if [[ -z $fl ]] 
	then 
	echo "ERROR:  functional-logger policy does not exist"
	bool=1
	boolApiPolicies=1 
	fi
	echo "The policies exists, now it is time to test if they have the correct order"
	firstlogger=$(cat api2.yaml | yq .xibmconfiguration.assembly.execute | grep "technical-logger" -n | head -n 1 | cut -d\: -f1)
	echo "Fist logger $firstlogger"
	secondlogger=$(cat api2.yaml | yq .xibmconfiguration.assembly.execute | grep "technical-logger" -n | tail -n 1 | cut -d\: -f1)
	echo "Second logger: $secondlogger"
	sgoauth=$(cat api2.yaml | yq .xibmconfiguration.assembly.execute | grep "sg-oauth2" -n | head -n 1 | cut -d\: -f1)
	echo "sg-oauth2: $sgoauth"
	tokenjwt=$(cat api2.yaml | yq .xibmconfiguration.assembly.execute | grep "token-jwt" -n | head -n 1 | cut -d\: -f1)
	echo "token-jwt: $tokenjwt"
	header_manager=$(cat api2.yaml | yq .xibmconfiguration.assembly.execute | grep "header_manager" -n | head -n 1 | cut -d\: -f1)
	echo "header_manager: $header_manager"
	activitylogger=$(cat api2.yaml | yq .xibmconfiguration.assembly.execute | grep "activity-logger" -n | head -n 1 | cut -d\: -f1)
	echo "activity-logger: $activitylogger"
	echo "F: $firstlogger"
	echo "S: $sgoauth"
	firstlogger=`expr $firstlogger + 1`
	sgoauth=`expr $sgoauth + 1`
	echo "F: $firstlogger"
	echo "S: $sgoauth"
	var1=$(($firstlogger - $sgoauth))
	var2=$(($firstlogger - $tokenjwt))
	var3=$(($firstlogger - $header_manager))
	var4=$(($firstlogger - $activitylogger))
	if [ "$var1" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var2" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	boolApiPolicies=1
	elif [ "$var3" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var4" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	fi
	
	var1=$(($sgoauth - $tokenjwt))
	var2=$(($sgoauth - $header_manager))
	var3=$(($sgoauth - $activitylogger))
	var4=$(($sgoauth - $secondlogger))
	if [ "$var1" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var2" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var3" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var4" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	fi
	var1=$(($tokenjwt - $header_manager))
	var2=$(($tokenjwt - $activitylogger))
	var3=$(($sgoauth - $secondlogger))
	if [ "$var1" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var2" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var3" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var4" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	fi
	
	var2=$(($header_manager - $activitylogger))
	var3=$(($header_manager - $secondlogger))
	if [ "$var2" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	elif [ "$var3" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	fi
	var3=$(($activitylogger - $secondlogger))
	if [ "$var3" -gt "0" ]; then
		echo "ERROR: The order of the policies is bad"
		bool=1
	    boolApiPolicies=1
	fi	
	
	
	
	
	
	
#-------------------   
#Test api definitions
#-------------------
	echo "The order of the polcies is ok so let's validate the definitions"
	AppName=$(cat api2.yaml | yq '.definitions.errorMS.properties.AppName.type' | cut -d\" -f2)
	echo "AppName: $AppName"
	if [ "$AppName" != "string" ];
	then
		echo "ERROR: AppName must be an string"
		bool=1
	    boolApiDefinitions=1
	fi
	timestamp=$(cat api2.yaml | yq '.definitions.errorMS.properties.timestamp.type' | cut -d\" -f2)
	echo "Timestamp: $timestamp"
	if [ "$timestamp" != "long" ];
	then
		echo "ERROR: timestamp must be long"
		bool=1
	    boolApiDefinitions=1
	fi
	errorName=$(cat api2.yaml | yq '.definitions.errorMS.properties.errorName.type' | cut -d\" -f2)
	echo "ErrorName: $errorName"
	if [ "$errorName" != "string" ];
	then
		echo "ERROR: ErrorName must be long"
		bool=1
	    boolApiDefinitions=1
	fi
	status=$(cat api2.yaml | yq '.definitions.errorMS.properties.status.type' | cut -d\" -f2)
	echo "Status: $status"
	if [ "$status" != "integer" ];
	then
		echo "ERROR: Status must be long"
		bool=1
	    boolApiDefinitions=1
	fi
	internalCode=$(cat api2.yaml | yq '.definitions.errorMS.properties.internalCode.type' | cut -d\" -f2)
	echo "Internal code: $internalCode"
	if [ "$internalCode" != "integer" ];
	then
		echo "ERROR: Internalcode must be long"
		bool=1
	    boolApiDefinitions=1
	fi
	shortMessage=$(cat api2.yaml | yq '.definitions.errorMS.properties.shortMessage.type' | cut -d\" -f2)
	echo "ShortMessage: $shortMessage"
	if [ "$shortMessage" != "string" ];
	then
		echo "ERROR: ShortMessage must be string"
		bool=1
	    boolApiDefinitions=1
	fi
	detailedMessage=$(cat api2.yaml | yq '.definitions.errorMS.properties.detailedMessage.type' | cut -d\" -f2)
	echo "DetailedMessage: $detailedMessage"
	if [ "$detailedMessage" != "string" ];
	then
		echo "ERROR: DetailedMessage must be string"
		bool=1
	    boolApiDefinitions=1
	fi
	httpCode=$(cat api2.yaml | yq '.definitions.errorZuul.properties.httpCode.type' | cut -d\" -f2)
	echo "httpCode: $httpCode"
	if [ "$httpCode" != "integer" ];
	then
		echo "ERROR: httpCode must be integer"
		bool=1
	    boolApiDefinitions=1
	fi
	httpMessage=$(cat api2.yaml | yq '.definitions.errorZuul.properties.httpMessage.type' | cut -d\" -f2)
	echo "httpMessage: $httpMessage"
	if [ "$httpMessage" != "string" ];
	then
		echo "ERROR: httpMessage must be string"
		bool=1
	    boolApiDefinitions=1
	fi
	moreInformation=$(cat api2.yaml | yq '.definitions.errorZuul.properties.moreInformation.type' | cut -d\" -f2)
	echo "moreInformation: $moreInformation"
	if [ "$moreInformation" != "string" ];
	then
		echo "ERROR: moreInformation must be string"
		bool=1
	    boolApiDefinitions=1
	fi
	errorResponse=$(cat api2.yaml | yq '.definitions.errorResponse')
	echo "errorResponse: $errorResponse"
	if [[ -z $errorResponse ]]
	then
		echo "ERROR: errorResponse must exists"
		bool=1
	    boolApiDefinitions=1
	fi
	
	
	
	
	
	
#-----------------------------   
#Test api security definitions
#-----------------------------
	echo "--- Let's validate the security definitions ---"
	oau=$(cat api2.yaml | yq '.securityDefinitions' | grep "Serenity-OAuth")
	echo "*** $oau"
	if [[ -z $oau ]]
	then
		echo "ERROR: Does not exists Serenity-OAuth"
		bool=1
	    boolApiSecurityDefinitions=1
	fi
	find ./api2.yaml -type f -exec sed -i 's/Serenity-OAuth/SerenityOAuth/g' {} +
	type=$(cat api2.yaml | yq '.securityDefinitions.SerenityOAuth.type' | cut -d\" -f2)
	echo "securityDefinitionstype: $type"
	if [ "$type" != "oauth2" ]
	then
		echo "ERROR: securityDefinitions type must be oauth 2"
		bool=1
	    boolApiSecurityDefinitions=1
	fi
	authorizationUrl=$(cat api2.yaml | yq '.securityDefinitions.SerenityOAuth.authorizationUrl' | cut -d\" -f2)
	echo "authorizationUrl: $authorizationUrl"
	if [ "$authorizationUrl" != "https://\$(SOSgateway)/authorize" ]
	then
		echo "ERROR: authorizationUrl must be 'https://\$(SOSgateway)/authorize'"
		bool=1
	    boolApiSecurityDefinitions=1
	fi
	scopes=$(cat api2.yaml | yq '.securityDefinitions.SerenityOAuth.scopes' | cut -d\" -f2)
	echo "scopes: $scopes"
	if [[ -z "$scopes" ]]
	then
		echo "ERROR: scopes must exists"
		bool=1
	    boolApiSecurityDefinitions=1
	fi
	
	tokenURL=$(cat api2.yaml | yq '.securityDefinitions.SerenityOAuth.tokenUrl' | cut -d\" -f2)
	echo "tokenURL: $tokenURL"
	if [ "$tokenURL" != "https://\$(SOSgateway)/token" ]
	then
		echo "ERROR: tokenUrl must be https://\$(SOSgateway)/token"
		bool=1
	    boolApiSecurityDefinitions=1
	fi
	flow=$(cat api2.yaml | yq '.securityDefinitions.SerenityOAuth.flow' | cut -d\" -f2)
	echo "flow=$flow"
	if [ "$flow" != "Implicit" -a "$flow" != "accessCode" -a "$flow" != "Password" -a "$flow" != "accessCode" -a "$flow" != "Application" ]
	then
		echo "Igual"
		bool=1
	    boolApiSecurityDefinitions=1
	fi
	
	
	
	
#------------------------------
# See results of the validation
#------------------------------	
if [ "$bool" == "1" ]
then
	echo "The api yaml has the following errors: "
	if [ "$boolVersion" == "1" ]
	then
		echo "The version in the yaml does not have the correct format"
	fi
	if [ "$boolApiProperties" == "1" ]
	then
		echo "The api yaml does not have all the properties it needs"
	fi
	if [ "$boolApiPolicies" == "1" ]
	then
		echo "The api yaml does not have all the policies needed or it has it but not in the correct order"
	fi
	if [ "$boolApiDefinitions" == "1" ]
	then
		echo "The api yaml does not have all the definitions needed"
	fi
	if [ "$boolApiSecurityDefinitions" == "1" ]
	then
		echo "The api yaml does not have all the security definitions needed"
	fi
	exit 1
fi
echo "The api yaml is correct"
rm api2.yaml
exit 0